package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"sse/server"
)

func main() {
	//SSE server
	s := server.New()

	r := mux.NewRouter()
	r.HandleFunc("/infocenter/{topic}", s.Subscribe()).Methods("GET")
	r.HandleFunc("/infocenter/{topic}", s.Send()).Methods("POST")
	log.Fatal("HTTP server error: ", http.ListenAndServe(":4000", r))
}
