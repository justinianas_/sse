package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"
)

const ClientOpen = 1
const ClientClose = 2

type (
	MessageChan chan []byte
	Message struct {
		topic 			*Topic
		text  			[]byte
	}
	Event struct {
		topic 			*Topic
		e     			int
		Chan  			MessageChan
	}
	Topic struct {
		clients 		map[MessageChan]bool
		name 			string
	}
	Server struct {
		topics       	map[string]*Topic
		notifier     	chan Message
		chanNotifier 	chan Event
		lastID       	int
		mux          	*sync.Mutex
	}
)

func New() (server *Server) {
	server = &Server{
		topics:       make(map[string]*Topic, 0),
		notifier:     make(chan Message),
		chanNotifier: make(chan Event),
		lastID:       0,
		mux:          &sync.Mutex{},
	}

	go server.listen()
	return
}

func (s *Server) incrementID() {
	s.mux.Lock()
	defer s.mux.Unlock()
	s.lastID++
}

func (s *Server) getID() int {
	s.mux.Lock()
	defer s.mux.Unlock()
	return s.lastID
}

func (s *Server) getTopic(topic string) *Topic {
	s.mux.Lock()
	defer s.mux.Unlock()
	if _, ok := s.topics[topic]; !ok {
		t := &Topic{clients: make(map[MessageChan]bool), name: topic}
		s.topics[topic] = t
	}
	return s.topics[topic]
}

func (s *Server) listen() {
	for {
		select {
		case v := <-s.chanNotifier:
			if v.e == ClientOpen {
				v.topic.clients[v.Chan] = true
				log.Printf("Client added. %d registered clients in '%s' topic.", len(v.topic.clients), v.topic.name)
			} else if v.e == ClientClose && v.topic.clients[v.Chan] {
				delete(v.topic.clients, v.Chan)
				log.Printf("Client removed. %d registered clients in '%s' topic.", len(v.topic.clients), v.topic.name)
				if len(v.topic.clients) == 0 {
					delete(s.topics, v.topic.name)
				}
			}
		case message := <-s.notifier:
			s.incrementID()
			for messageChan, _ := range message.topic.clients {
				messageChan <- message.text
			}
		}
	}
}

func (s *Server) Subscribe() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		t := vars["topic"]
		topic := s.getTopic(t)

		flusher, ok := w.(http.Flusher)
		if !ok {
			http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		messageChan := make(MessageChan)
		s.chanNotifier <- Event{topic: topic, e: ClientOpen, Chan: messageChan}

		defer func() {
			s.chanNotifier <- Event{topic: topic, e: ClientClose, Chan: messageChan}
		}()

		notify := r.Context().Done()
		go func() {
			<-notify
			s.chanNotifier <- Event{topic: topic, e: ClientClose, Chan: messageChan}
		}()

		timeout := time.After(30 * time.Second)
		for {
			select {
			case v := <-messageChan:
				_, _ = fmt.Fprintf(w, "id: %s\nevent: msg\ndata: %s\n\n", strconv.Itoa(s.getID()), v)
			case <-timeout:
				_, _ = fmt.Fprintf(w, "event: timeout\ndata: 30s\n\n")
				s.chanNotifier <- Event{topic: topic, e: ClientClose, Chan: messageChan}
			}
			flusher.Flush()
		}
	}
}

func (s *Server) Send() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		topic := vars["topic"]

		defer r.Body.Close()
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Can't parse request body", http.StatusBadRequest)
		}
		s.notifier <- Message{topic: s.getTopic(topic), text: data}

		w.WriteHeader(http.StatusNoContent)
		return
	}
}
